@servers(['web' => 'apotikroxy@app-sandbox.xyz'])

@setup
    $repository = 'git@gitlab.com:dev_roxy/apotex_roxy_backend.git';
    $releases_dir = '/www/wwwroot/web/test/releases';
    $app_dir = '/www/wwwroot/web/test';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    update_permission
    remove_older_directory
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer update --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('update_permission')
    echo "Change Permission Directory"
    sudo chown -R www /www/wwwroot/web/test/current/bootstrap
@endtask

@task('remove_older_directory')
    echo "Remove Older Directory"
    $files1 = scandir( {{ $releases_dir }} );
    @foreach($files1 as $row)
        if($row!=$release){
            rm -rf {{ $row }}
        }
    @endforeach
@endtask

@finished
    if ($exitCode > 0) {
         @telegram('5721953321:AAHK5J0PIpYT5iGQHU4EEbjzvlXf9Coy9S0','60113097', 'Halo Team, ada update terbaru dari tim Back-End, bisa dicek yah di url https://api.app-sandbox.xyz')
    }
@endfinished